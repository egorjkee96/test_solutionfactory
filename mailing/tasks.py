from celery import shared_task
from celery.utils.log import get_task_logger
from django.db.models import Q
from datetime import datetime, date
from . import models
from .utils import send_email, send_sms

logger = get_task_logger(__name__)


@shared_task
def mailing_task():
    now = datetime.utcnow()
    mailing = models.Mailing.objects.filter(datetime_start__lte=now, datetime_end__gte=now, in_proccess=False).first()
    if mailing is None:
        return
    mailing.in_proccess = True
    mailing.save()
    query = Q()
    if mailing.filter['tag'] is not None:
        query &= Q(tag=mailing.filter['tag'])
    if mailing.filter['phone_code'] is not None:
        query &= Q(phone_code=mailing.filter['phone_code'])
    clients = list(models.Client.objects.filter(query))
    while len(clients) != 0:
        client = clients.pop()
        if datetime.utcnow() > mailing.datetime_end:
            clients.append(client)
            for client in clients:
                models.Message.objects.create(status="time is over", mailing=mailing, client=client)
            return
        resp = send_sms(id=mailing.id, phone_number=client.phone_number, text=mailing.text)
        if resp is True:
            models.Message.objects.create(status="success", mailing=mailing, client=client)
        else:
            clients.append(client)


@shared_task
def email_statistic_task():
    today = datetime.utcnow()
    today_messages = models.Message.objects.filter(datetime__lte=today)
    if len(today_messages) == 0:
        return 
    today_mailing = set([i.mailing_id for i in today_messages])
    success_messages = [i for i in today_messages if i.status == 'success']
    time_is_over_messages = [i for i in today_messages if i.status == 'time is over']
    text = f"{today.strftime('%m/%d/%Y')}\n\n"  \
           f"Всего рассылок за сегодня: {len(today_mailing)}\n"  \
           f"Всего сообщений отправлено: {len(today_messages)}\n"  \
           f"Успешно отправленных сообщений: {len(success_messages)}\n"  \
           f"Сообщения которые были не отправленны из за окончания времени рассылки: {len(time_is_over_messages)}"
    send_email(text)  
