from django.urls import path
from rest_framework import routers, serializers, viewsets

from . import views




router = routers.DefaultRouter()
router.register(r'clients', views.ClientViewSet)
router.register(r'mailing', views.MailingViewSet)
router.register(r'message', views.MessageViewSet)

