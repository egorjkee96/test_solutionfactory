from timezone_field.rest_framework import TimeZoneSerializerField
from rest_framework import serializers
from . import models



class ClientSerializer(serializers.ModelSerializer):
    tz = TimeZoneSerializerField(use_pytz=True, allow_null=True)
    class Meta:
        model = models.Client
        fields = ['id', 'phone_number', 'tag', 'tz']
        read_only_fields = ('id',)


class FilterSerializer(serializers.Serializer):
    tag = serializers.CharField(allow_null=True)
    phone_code = serializers.IntegerField(allow_null=True)


class MailingSerializer(serializers.ModelSerializer):
    filter = FilterSerializer()
    class Meta:
        model = models.Mailing
        fields = ['id', 'text', 'datetime_start', 'datetime_end', 'filter']
        read_only_fields = ('id',)


class MessageSerializer(serializers.ModelSerializer):
    mailing = MailingSerializer()
    client = ClientSerializer()
    
    class Meta:
        model = models.Message
        fields = ['id', 'datetime', 'status', 'mailing', 'client']

class MessageMailingSerializer(MessageSerializer):
    mailing = None