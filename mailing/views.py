from . import models
from . import serializers
from rest_framework.decorators import action
from rest_framework import viewsets, mixins
from rest_framework.response import Response


class ClientViewSet(viewsets.ModelViewSet):
    """Для добавления TZ используйте формат - 'Europe/Moscow'"""
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer



class MailingViewSet(viewsets.ModelViewSet):
    queryset = models.Mailing.objects.all()
    serializer_class = serializers.MailingSerializer

    @action(detail=True)
    def statistic(self, request, pk):
        messages = models.Message.objects.filter(mailing_id=pk)
        mailing = models.Mailing.objects.get(pk=pk)
        message_serializer = serializers.MessageMailingSerializer(messages, many=True)
        mailing_serializer = serializers.MailingSerializer(mailing, many=False)
        return Response({"mailing": mailing_serializer.data ,"messages": message_serializer.data})


class MessageViewSet(mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    queryset = models.Message.objects.all()
    serializer_class = serializers.MessageSerializer