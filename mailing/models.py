from datetime import datetime
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from timezone_field import TimeZoneField


class Mailing(models.Model):
    text = models.TextField()
    filter = models.JSONField()
    datetime_start = models.DateTimeField(default=datetime.now())
    datetime_end = models.DateTimeField()
    in_proccess = models.BooleanField(default=False)

    class Meta:
        db_table = "mailing"

    def __str__(self) -> str:
        return f"{self.text}|start:{self.datetime_start.strftime('%m/%d/%Y, %H:%M:%S')}|end:{self.datetime_end.strftime('%m/%d/%Y, %H:%M:%S')}"


def validate_number(value: str):
    raise_string = None
    if value[0] != '7':
        raise_string = "Номер должен начинаться с 7."
 
    if value.isdigit() is False:
        raise_string = 'Номер должен состоять только из цифр.'
    if len(value) != 11:
        raise_string = "Номер состоит из 11 цифр."

    if raise_string:
        raise ValidationError(
            _(raise_string),
            params={'value': value},
        )

class Client(models.Model):
    phone_number = models.CharField(max_length=11 ,validators=[validate_number], unique=True)
    phone_code = models.IntegerField()
    tag = models.TextField()
    tz = TimeZoneField(use_pytz=True, null=True)

    class Meta:
        db_table = "client"

    def __str__(self) -> str:
        return f"{self.phone_number}"


    def save(self, *args, **kwargs):
        self.phone_code = int(self.phone_number[1:4])
        return super(Client, self).save(*args, **kwargs)


class Message(models.Model):
    datetime = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=30)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)

    class Meta:
        db_table = "message"
    
