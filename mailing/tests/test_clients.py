from django.test import TestCase, Client
from rest_framework import status
from rest_framework.reverse import reverse
from .. import models
from .. import serializers
import json

api_client = Client()

class GetAllCleintsTest(TestCase):
    def setUp(self):
        models.Client.objects.create(phone_number="79006292609", tag="tag1")
        models.Client.objects.create(phone_number="79006292601", tag="tag1")
        models.Client.objects.create(phone_number="79006292602", tag="tag1")
        models.Client.objects.create(phone_number="79006292603", tag="tag1")

    def test_get_all_clients(self):
        response = api_client.get(reverse("client-list"))
        clients = models.Client.objects.all()
        serializer = serializers.ClientSerializer(clients, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleClientTest(TestCase):
    def setUp(self):
        self.client_1 = models.Client.objects.create(phone_number="79006292609", tag="tag1")
        self.client_2 = models.Client.objects.create(phone_number="79006292601", tag="tag1")
        self.client_3 = models.Client.objects.create(phone_number="79006292602", tag="tag1")
        
    def test_get_valid_single_client(self):
        response = api_client.get(reverse('client-detail', kwargs={'pk': self.client_1.pk}))
        client = models.Client.objects.get(pk=self.client_1.pk)
        serializer = serializers.ClientSerializer(client)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_client(self):
        response = api_client.get(reverse('client-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewClientTest(TestCase):
    def setUp(self):
        self.valid_payload = {
            "phone_number":"79006292609", 
            "tag":"tag1",
            "tz": "Europe/Moscow"}
        self.invalid_payloads = [{
            "phone_number":"99006292609", 
            "tag":"tag1"
        }, 
        {
            "phone_number":"9006292609", 
            "tag":"tag1"
        },
        {
            "phone_number":"79006292609", 
            "tag":"tag1",
            "tz": "Scw"
        },
        {
            "phone_number":"79006292609", 
            "tag":"tag1"
        }
        ]

    def test_create_valid_client(self):
        response = api_client.post(
            reverse('client-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_invalid_client(self):
        for invalid_payload in self.invalid_payloads:
            response = api_client.post(
                reverse('client-list'),
                data=json.dumps(invalid_payload),
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)



class UpdateSingleClientTest(TestCase):
    def setUp(self):
        self.client_1 = models.Client.objects.create(phone_number="79006292609", tag="tag1", tz="UTC")
        self.client_2 = models.Client.objects.create(phone_number="79006292600", tag="tag1")
        self.valid_payload = {
            'phone_number': '79216523452',
            'tag': "tag2",
            'tz': 'Europe/Moscow'
        }
        self.invalid_payload = {
            'phone_number': '79216525',
            'tag': "tag1",
            'tz': 'Europe/Moscow'
        }
    def test_valid_update_client(self):
        response = api_client.put(
            reverse('client-detail', kwargs={'pk': self.client_1.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_client(self):
        response = api_client.put(
            reverse('client-detail', kwargs={'pk': self.client_1.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleClientTest(TestCase):
    def setUp(self):
        self.client_1 = models.Client.objects.create(phone_number="79006292609", tag="tag1", tz="UTC")
        self.client_2 = models.Client.objects.create(phone_number="79006292601", tag="tag1", tz="UTC")
    
    def test_valid_delete_client(self):
        response = api_client.delete(
            reverse('client-detail', kwargs={'pk': self.client_1.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_client(self):
        response = api_client.delete(
            reverse('client-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)



