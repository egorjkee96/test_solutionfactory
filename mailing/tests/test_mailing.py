from datetime import datetime
from django.test import TestCase, Client
from rest_framework import status
from rest_framework.reverse import reverse
from .. import models
from .. import serializers
import json

api_client = Client()


class GetAllMailingTest(TestCase):
    def setUp(self):
        models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))

    def test_get_all_mailing(self):
        response = api_client.get(reverse("mailing-list"))
        clients = models.Mailing.objects.all()
        serializer = serializers.MailingSerializer(clients, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)



class GetSingleMailingTest(TestCase):
    def setUp(self):
        self.mailing_1 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        
        self.mailing_2 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))

    def test_get_valid_single_mailing(self):
        response = api_client.get(reverse('mailing-detail', kwargs={'pk': self.mailing_1.pk}))
        mailing = models.Mailing.objects.get(pk=self.mailing_1.pk)
        serializer = serializers.MailingSerializer(mailing)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_mailing(self):
        response = api_client.get(reverse('mailing-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CreateNewMailingTest(TestCase):
    def setUp(self):
        self.valid_payload = {
                                "text": "text",
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "datetime_end": "2022-07-30T15:23:13.599668",
                                "filter": {
                                    "tag": "tag",
                                    "phone_code": 900
                                }
                            }
        self.invalid_payloads = [{
                                "text": "text",
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "filter": {
                                    "tag": "tag",
                                    "phone_code": "905"
                                }
                            },
                            {
                                "text": "text",
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "datetime_end": "2022-07-30T15:23:13.599668",
                            },
                            {
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "datetime_end": "2022-07-30T15:23:13.599668",
                                "filter": {
                                    "tag": "tag",
                                    "phone_code": 900
                                }
                            }]

    def test_create_valid_mailing(self):
        response = api_client.post(
            reverse('mailing-list'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    
    def test_create_invalid_mailing(self):
        for invalid_payload in self.invalid_payloads:
            response = api_client.post(
                reverse('mailing-list'),
                data=json.dumps(invalid_payload),
                content_type='application/json'
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)



class UpdateSingleMailingTest(TestCase):
    def setUp(self):
        self.mailing_1 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        
        self.mailing_2 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        self.valid_payload = {
                                "text": "text",
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "datetime_end": "2022-07-30T15:23:13.599668",
                                "filter": {
                                    "tag": "tag",
                                    "phone_code": 900
                                }
        }
        
        self.invalid_payload = {
                                "text": "text",
                                "datetime_start": "2022-07-30T14:23:13.599668",
                                "filter": {
                                    "tag": "tag",
                                    "phone_code": "905"
                                }
                            }
        
    def test_valid_update_mailing(self):
        response = api_client.put(
            reverse('mailing-detail', kwargs={'pk': self.mailing_1.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_update_mailing(self):
        response = api_client.put(
            reverse('mailing-detail', kwargs={'pk': self.mailing_1.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class DeleteSingleClientTest(TestCase):
    def setUp(self):
        self.mailing_1 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
        
        self.mailing_2 = models.Mailing.objects.create(text="text", 
                                      filter={"tag": "tag", 
                                              "phone_code": 123}, 
                                      datetime_start=datetime(year=2020, 
                                                              month=6,
                                                              day=7,
                                                              hour=0,
                                                              minute=0,
                                                              second=0),
                                      datetime_end=datetime(year=2020, 
                                                              month=6,
                                                              day=8,
                                                              hour=0,
                                                              minute=0,
                                                              second=0))
    
    def test_valid_delete_mailing(self):
        response = api_client.delete(
            reverse('mailing-detail', kwargs={'pk': self.mailing_1.pk}))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_invalid_delete_mailing(self):
        response = api_client.delete(
            reverse('mailing-detail', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)