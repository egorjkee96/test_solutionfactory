import requests
from django.conf import settings
import smtplib                                      # Импортируем библиотеку по работе с SMTP
import os

# Добавляем необходимые подклассы - MIME-типы
from email.mime.multipart import MIMEMultipart      # Многокомпонентный объект
from email.mime.text import MIMEText                # Текст/HTML
from email.mime.image import MIMEImage      

def send_sms(id: int, 
             phone_number: str,
             text: str):
    headers = {"Authorization": settings.API_TOKEN}
    data = {
            "id": id,
            "phone": int(phone_number),
            "text": text
            }
    
    try:
        resp = requests.post(url=f"https://probe.fbrq.cloud/v1/send/{id}", headers=headers, json=data, timeout=10)
        if resp.status_code == 200 and resp.json()["message"] == "OK":
            return True
        else:
            return False
    except requests.exceptions.ReadTimeout:
        return False

    

# eDK60J41&nSm
def send_email(text: str):
    addr_from = settings.EMAIL_ADDR_FROM                # Адресат
    addr_to = settings.EMAIL_ADDR_TO
    password  = settings.EMAIL_PASSWORD                                 # Пароль
    # password  = "iphone95"                                  # Пароль
    msg = MIMEMultipart()                               # Создаем сообщение
    msg['From']    = addr_from                          # Адресат
    msg['To']      = addr_to                            # Получатель
    msg['Subject'] = 'Статистика'
    
    body = text

    msg.attach(MIMEText(body, 'plain'))                 # Добавляем в сообщение текст
    ctype = 'application/octet-stream'  # Будем использовать общий тип
    server = smtplib.SMTP_SSL('smtp.mail.ru:465')         # Создаем объект SMTP
    server.set_debuglevel(True)                         # Включаем режим отладки - если отчет не нужен, строку можно закомментировать
    server.login(addr_from, password)                 # Получаем доступ
    
    server.send_message(msg)                            # Отправляем сообщение
    server.quit()    