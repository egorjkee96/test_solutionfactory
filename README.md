Запуск конейнеров - sudo docker-compose -f docker-compose.yml up

Создание суперюзера для админки - sudo docker-compose exec web python manage.py createsuperuser

Админка по адресу 0.0.0.0:8000/admin/

Swagger 0.0.0.0:8000/docs/