import os

from celery import Celery
from celery.schedules import crontab


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mass_mailing.settings")

app = Celery("mass_mailing")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()



app.conf.beat_schedule = {
    'mailing_task' :{
        'task': 'mailing.tasks.mailing_task',
        'schedule' : crontab(minute='*/1'),
    },
    'email_statistic_task' :{
        'task': 'mailing.tasks.email_statistic_task',
        'schedule' : crontab(minute=0, hour=23),
    }
}